const express = require("express");
const router = express.Router();
const auth = require("../auth.js");

const courseController = require("../Controllers/courseController.js");

// Route for creating a course
router.post("/", auth.verify, courseController.addCourse);

//Route for retrieving all courses
router.get("/all", auth.verify, courseController.allCourses);

//Route for retrieving all active courses
router.get("/allActive", courseController.allActiveCourses)

//Route for retrieving all inactive courses
router.get("/allInactive", auth.verify, courseController.allInactiveCourses)

//[Routes with Params]

//Route for retrieving specific course
router.get("/:courseId", courseController.courseDetails);

router.put("/update/:courseId", auth.verify, courseController.updateCourse);

router.patch("/archive/:courseId", auth.verify, courseController.archiveCourse);

module.exports = router;